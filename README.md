# MY CONFIGS

Sources of Inspiration:

https://gitlab.com/dwt1/dotfiles

https://github.com/antoniosarosi/dotfiles

https://github.com/BrodieRobertson/dotfiles

- Change grub theme
  Download CyberRe theme : https://www.gnome-look.org/p/1420727/

Extract it and
sudo mv CyberRe /boot/grub/themes/

Edit /etc/default/grub and add:
GRUB_THEME=/boot/grub/themes/CyberRe/theme.txt

sudo update-grub
