" General
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/general/keys.vim
source $HOME/.config/nvim/general/plugins.vim

let g:coc_snippet_next = '<tab>'

let s:root = expand('<sfile>:h')

if exists('g:vscode')
    source s:root.'/.config/nvim/vscode/settings.vim'
else
    " Source every file in the path '.config/nvim/plug-config'
    let s:plugsdir = s:root.'/plug-config/'
    for fpath in split(globpath(s:plugsdir, '*.vim'), '\n')
      exe 'source' fpath
    endfor
endif
