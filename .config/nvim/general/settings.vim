" Search options
set hlsearch
set ignorecase
set incsearch
set smartcase

" Scrolloff options
set scrolloff=1
set sidescrolloff=5

" Set nowrap option
set nowrap

" Show the cursor's position at the bottom of the screen
set ruler

" Completion options
set wildmenu
set complete+=kspell

" Indent options
set smartindent
set autoindent

" Tabs
set expandtab
set tabstop=4 shiftwidth=4 expandtab

" Open new buffers without saving current modifications
set hidden

" Highlight the line of the cursor
set cursorline

" Numbers options
set relativenumber
set number

" Enable the terminal mouse controller
set mouse=a

" Set the title to the document's name
set title

" Utilize the main clipboard
set clipboard=unnamedplus

" Set large history
set history=1000

" Set no swapfile
set noswapfile

" Set no backup
set nobackup

" Set undofile in ~/.vim/undodir
set undodir=~/.config/nvim/undodir
set undofile

" Split Options
set splitbelow
set splitright

" Use Unicode
set encoding=utf-8
set fileencoding=utf-8

" Use the system's clipboard
set clipboard=unnamedplus

" Set the dark background
set background=dark

" Auto Chdir to the file's dir
set autochdir

" Set keymap to " "
set keymap=" "

" Syntax highlighting options
syntax enable
filetype plugin indent on

" Show hidden charactyers
set list

" Do not act like vi
set nocompatible
" Searches current directory recursively.
set path+=**

" Always show statusline
set laststatus=2

" Gui options
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar


set t_Co=256                    " Set if term supports 256 colors.
set colorcolumn=80

" Give more space for the messages
set cmdheight=2

set updatetime=50

"" Turn Caps off if they're on when switching to normal mode
"https://vi.stackexchange.com/questions/376/can-vim-automatically-turn-off-capslock-when-returning-to-normal-mode
au InsertLeave * call TurnOffCaps() " In the Insert leave event
function TurnOffCaps()  
    let capsState = matchstr(system('xset -q'), '00: Caps Lock:\s\+\zs\(on\|off\)\ze')
    if capsState == 'on'
        execute ':!xdotool key Caps_Lock'
    endif
endfunction

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
