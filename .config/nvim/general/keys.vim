""" THIS KEYS CONFIG HAS NICE SHORCUTS, TO AVOID USING
""" THE DEFAULT SHORCUTS INSTEAD OF THE ONES MAPPED,
""" THE ORIGINALS ARE MAPPED TO TEXT PRINTING

let mapleader = " "

"""""" BASIC REMAP """"""
" Remap kj to escape
inoremap kj <C-c>
inoremap KJ <C-c>

cnoremap kj <C-c>
cnoremap KJ <C-c>

xnoremap kj <C-c>
xnoremap KJ <C-c>

tnoremap jk <C-\><C-n>
tnoremap JK <C-\><C-n>

vnoremap kj <C-c>
vnoremap KJ <C-c>


" Remaap 'df' to :
nmap df :
cmap df <C-c>
"""""" END BASIC REMAP """"""

" Alternate way to save
nmap <Leader>w :w<CR>

" Quit without saving
nmap <Leader><Leader>q :q!<CR>

"" Alternate way to quit and save
nmap <Leader>q :conf q<CR>

" Session
nmap <Leader>se <C-c>:mksession ~/.config/nvim/sessions/.vim<Left><Left><Left><Left>
" Session Restore
nmap <Leader>sr <C-c>:source ~/.config/nvim/sessions/
" Session Delete
nmap <Leader>sd <C-c>:!rm -f ~/.config/nvim/sessions/

" Source automatically
nmap <Leader>sc :source ~/.config/nvim/init.vim<CR>

" PlugInstall
nmap <Leader>pi :PlugInstall <bar> :vertical resize 60<CR>
" PlugClean
nmap <Leader>pc :PlugClean <bar> :vertical resize 60<CR>
" Plug Update Plugins
nmap <Leader>pup :PlugUpdate <bar> :vertical resize 60<CR>
" Plug Update Self
nmap <Leader>pus :PlugUpgrade <bar> :vertical resize 60<CR>
" Plug Update COC
nmap <Leader>puc :CocUpdate<CR>


nmap oo o<C-c>0d$
nmap OO O<C-c>0d$

"""""" END BASIC REMAP """"""

"""""" WINDOWS """"""
" Better window navigation
nmap <silent> <Leader>k :wincmd k<CR>
nmap <silent> <Leader>j :wincmd j<CR>
nmap <silent> <Leader>l :wincmd l<CR>
nmap <silent> <Leader>h :wincmd h<CR>

" Move current window to another position
nmap <silent> <Leader>K :wincmd K<CR>
nmap <silent> <Leader>J :wincmd J<CR>
nmap <silent> <Leader>L :wincmd L<CR>
nmap <silent> <Leader>H :wincmd H<CR>

" Use alt + hjkl to resize windows
nmap <silent> <M-j> :resize -2<CR>
nmap <silent> <M-k> :resize +2<CR>
nmap <silent> <M-h> :vertical resize -2<CR>
nmap <silent> <M-l> :vertical resize +2<CR>

" Make window's sizes equal
nmap <M-=> <C-w>=

""" Split easily
" Vertical Split
nmap <Leader>v :vs<Space>
nmap <Leader><Leader>v :vs %<CR>

" Horizontal Split
nmap <Leader>s :sp<Space>
nmap <Leader><Leader>s :sp %<CR>
"""""" END WINDOWS """"""

nmap <Leader>t :vs<bar> :terminal<CR>
nmap <Leader>T :sp<bar> :terminal<CR>

nmap > :bnext<CR>
nmap < :<CR>

cmap <C-j> <C-n>
cmap <C-k> <C-p>
cmap <C-h> <Left>
cmap <C-l> <Right>

imap <C-j> <C-n>
imap <C-k> <C-p>
"""""" YANKING/PASTING/CUTTING CONFIGS """"""
" Disable cutting when deleting with x and dw
nnoremap x "_x
xnoremap x "_x
nnoremap dw "_dw
xnoremap dw "_dw
" i c n x v

" Surrounding keybindings
nnoremap ds v%d
nnoremap dS <S-v>$%d
nnoremap ys v%y
nnoremap yS <S-v>$%y

"""" END YANKING/PASTING/CUTTING CONFIGS """"""


"""""" PLUGINS SHORTCUTS """"""

""" Undotree
" Show
nmap <Leader>u :UndotreeShow<CR>
" Hide
nmap <Leader>U :UndotreeHide<CR> 

" Project View opens NerdTree
nmap <silent> <C-n> :NERDTreeToggle<CR>


" Find (file)
nmap <Leader>ff :Telescope find_files<CR>
" Fing (grep)
nmap <Leader>fg :Telescope live_grep<CR>

""" COC
" Go to Definition
nmap <silent> <Leader>gd <Plug>(coc-definition)
nmap <silent> <Leader>gi <Plug>(coc-implementation)
nmap <silent> <Leader>gr <Plug>(coc-references)
nmap <silent> <Leader>gn <Plug>(coc-rename) " name

" Format
" Imports
nmap <silent> <Leader>fi :call CocAction('runCommand', 'editor.action.organizeImport')<CR>

" List
" Outlines
nmap <silent> <Leader>lo :<C-u>CocList outline<CR>
" diagnostics
nmap <silent> <Leader>ld :<C-u>CocList diagnostics<CR>

" i c n x v
map <C-f> <Plug>(easymotion-bd-f2)
nmap <C-f> <Plug>(easymotion-overwin-f2)


map  / <Plug>(easymotion-sn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

map <C-k> <Plug>(easymotion-k)
nmap <C-k> <Plug>(easymotion-k)

map <C-j> <Plug>(easymotion-j)
nmap <C-j> <Plug>(easymotion-j)

map <C-l> <Plug>(easymotion-bd-e)
nmap <C-l> <Plug>(easymotion-bd-e)

map <C-h> <Plug>(easymotion-bd-w)
nmap <C-h> <Plug>(easymotion-bd-w)

" Vim-Which-Key
nmap <silent> <leader> :WhichKey '<Space>'<CR>
set timeoutlen=300

" NerdCommenter DON'T FORGET THE SHORTCUTS FOR THE NERD COMMENTER:
" <Leader>ci INVERT COMMENTS OF LINES INDIVIDUALLY
" <Leader>c<Space> INVERT COMMENTS OF LINES
"""""" END PLUGINS SHORTCUTS """"""

nmap <silent> m :call <SID>show_documentation()<CR>
nnoremap <silent> K :call CocAction('doHover')<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
