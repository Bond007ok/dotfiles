call plug#begin('~/.config/nvim/plugged')
    if exists('g:vscode')
        " Easy motion for VSCode
        Plug 'asvetliakov/vim-easymotion'
    else
        " Lightline statusbar
        Plug 'hoob3rt/lualine.nvim'
        " Lightline statusbar

        """ UTILITIES """
        " Ripgrep
        Plug 'jremmen/vim-ripgrep'
        " DON'T ACTIVATE THE ONE BELOW
        "Plug 'vim-utils/vim-man'
        " NerdTree
        Plug 'preservim/nerdtree'
        " Nerd Commenter
        Plug 'preservim/nerdcommenter'
        " Undotree
        Plug 'mbbill/undotree'
        " ConquerorOfCompletion (COC)
        Plug 'neoclide/coc.nvim', {'branch': 'release'}
        " File finding
        Plug 'nvim-lua/popup.nvim'
        Plug 'nvim-lua/plenary.nvim'
        Plug 'nvim-telescope/telescope.nvim'
        " Git integration
        Plug 'airblade/vim-gitgutter'
        " Easy motion
        Plug 'easymotion/vim-easymotion'
        " Which key
        Plug 'liuchengxu/vim-which-key'
        " Vimspector
        Plug 'puremourning/vimspector'
        " Surround.vim
        Plug 'tpope/vim-surround'
        " Closetag.vim
        Plug 'alvan/vim-closetag'
        " Tab bar
        Plug 'romgrk/barbar.nvim'
        " Underlines the word you're on
        Plug 'yamatsum/nvim-cursorline'
        " Start up time meter
        Plug 'tweekmonster/startuptime.vim'
        " Treesitter
        Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
        """"" END UTILITIES

        """" LOOK """""
        """ SYNTAX MANIPULATION """
        "" HIGHLIGHTING ""
        " Vim-Polyglot for nice syntax highlighting
        Plug 'sheerun/vim-polyglot'
        " Highlight when you yank
        Plug 'machakann/vim-highlightedyank'
        " Nerdtree syntax highlighting
        Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
        " Nerdtree git
        Plug 'Xuyuanp/nerdtree-git-plugin' 
        " Vim Rainbox
        Plug 'frazrepo/vim-rainbow'
        " Vim Hex Preview color
        Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
        " Minimap
        Plug 'wfxr/minimap.vim'
        " Unintrusive peek of number
        Plug 'nacro90/numb.nvim'
        " Indenting lines
        Plug 'lukas-reineke/indent-blankline.nvim'
        " Session Management
        Plug 'folke/persistence.nvim'
        """" END HIGHLIGHTING ""

        " Vim devicons
        Plug 'ryanoasis/vim-devicons'
        Plug 'kyazdani42/nvim-web-devicons'

        " Dashboard
        Plug 'glepnir/dashboard-nvim'
        " Install Asynchronous Line Engine (ALE)
        Plug 'dense-analysis/ale'
        " Automatic bracket completion
        Plug 'jiangmiao/auto-pairs'
         "Startify
        "Plug 'mhinz/vim-startify'

        """ THEMES """
        Plug 'kaicataldo/material.vim'
        Plug 'folke/tokyonight.nvim'
        Plug 'morhetz/gruvbox'
        Plug 'joshdick/onedark.vim'
        Plug 'dracula/vim', { 'as': 'dracula' }
        """" END LOOK 
endif

call plug#end()

