" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

autocmd User EasyMotionPromptBegin silent! CocDisable
autocmd User EasyMotionPromptEnd silent! CocEnable
o
let g:EasyMotion_keys = 'abcdefghijklmnopqrstuvwxyz12347890'
