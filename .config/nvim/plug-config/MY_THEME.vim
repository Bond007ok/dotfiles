""""  Activate the gruvbox plugin """
"let g:gruvbox_contrast_dark = 'hard'
"colorscheme gruvbox
""""  END Activate the gruvbox plugin """

"""" Activate the dracula plugin """
"colorscheme dracula
"set background = "dark"
"""" END Activate the dracula plugin """

"""" Activate the material plugin """
"" 'default' | 'palenight' | 'ocean' | 'lighter' | 'darker'
"" 'palenight' | 'ocean'
"let g:material_terminal_italics = 1
"let g:material_theme_style = 'ocean'
"colorscheme material

"if (has('nvim'))
  "let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
"endif

"if (has('termguicolors'))
  "set termguicolors
"endif
"""" END Activate the materal plugin """

"""" Activate the Atom onedark plugin """"
"colorscheme onedark
""Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
""If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
""(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
"if (empty($TMUX))
  "if (has("nvim"))
    ""For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    "let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  "endif
  ""For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  ""Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  "" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  "if (has("termguicolors"))
    "set termguicolors
  "endif
"endif
"let g:onedark_terminal_italics=1
"let g:onedark_termcolors=256
"""" END Activate the Atom onedark plugin """"


"""" Activate the Tokyonight plugin """"
set termguicolors

let g:tokyonight_style = 'night' " available: night, storm
let g:tokyonight_enable_italic = 1
let g:tokyonight_italic_functions = 1

colorscheme tokyonight
"""" END Activate the Tokyonight plugin """"

""""" Activate the Tomorrow Theme """"
"source ./manual_themes/.vim
""""" END Activate the Tomorrow Theme """"
