lua << EOF
      require("indent_blankline").setup {
          space_char_blankline = " ",
          show_current_context = true,
      }
EOF

let g:indentLine_fileTypeExclude = ['dashboard']
autocmd FileType dashboard set showtabline=0 | autocmd WinLeave <buffer> set showtabline=2
