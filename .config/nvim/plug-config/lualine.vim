let g:lualine = {
    \'options' : {
    \  'theme' : 'ayu_mirage',
    \  'section_separators' : ['', ''],
    \  'component_separators' : ['', ''],
    \  'icons_enabled' : v:true,
    \},
    \'sections' : {
    \  'lualine_a' : [ ['mode', {'upper': v:true,},], ],
    \  'lualine_b' : [ ['diagnostics', {
    \       'sources': ['coc', 'ale'], 'color_error': '#ec5f67', 'color_warn': '#ECBE7B', 'color_info': '#008080', 'symbols': {'error': ' ', 'warn': ' ', 'info': ' '}}] ],
    \  'lualine_c' : [ ['filename', {'file_status': v:true, 'color': {'fg': '#ff5775', 'gui': 'bold'}, 'symbols': {'modified': '[+]', 'readonly': '[]'}},],
    \       ['filetype', {'color': {'fg': '#ff5775', 'gui': 'bold'}}] ],
    \  'lualine_x' : [ ['branch', {'icon': '', 'color': {'fg': '#c678dd', 'gui': 'bold'}}, ], ['diff', {
    \           'color_added': '#98be65', 'color_modified': '#FF8800', 'color_removed': '#ec5f67',
    \           'symbols': {'added': ' ', 'modified': '柳 ', 'removed': ' '}}]],
    \  'lualine_y' : [ ['encoding', {'color': {'fg': '#98be65', 'gui': 'bold'}}],
    \       ['fileformat', {'color': {'fg': '#98be65', 'gui': 'bold'}}]],
    \  'lualine_z' : [ 'location', 'progress' ],
    \},
    \'inactive_sections' : {
    \  'lualine_a' : [  ],
    \  'lualine_b' : [  ],
    \  'lualine_c' : [  ],
    \  'lualine_x' : [  ],
    \  'lualine_y' : [  ],
    \  'lualine_z' : [  ],
    \},
    \'extensions' : [ 'nerdtree' ],
    \}
lua require("lualine").setup()
