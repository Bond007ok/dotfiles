" Config for Nerdtree
" autocmd VimEnter * NERDTree | wincmd l
"let g:NERDTreeDirArrowExpandable = '►'
"let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1

" Enable the NerdreeHighlighting
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
let g:NERDTreeHighlightFolders = 1 " enables folder icon highlighting using exact match
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder name
let NERDTreeCustomOpenArgs = {'file': {'reuse': 'all', 'where': 'v', 'keepopen':0, 'stay':0}, 'dir':{}}
