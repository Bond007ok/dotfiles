#! /bin/bash

picom --config ~/.config/picom-jonaburg/picom.conf & # picom-jonaburg-git
dunst & # Notification
redshift -O 2400 & # Eye-care light
lxpolkit & # Set up the polkit

#### UPDATING THE INSTALLED SOFTWARE LIST LOCATED AT ~/.installed_software ####
pacman -Qqen > ~/.installed_software/pacman_explicitly_installed.txt & # Update the pacman explicitly installed list
pacman -Qqem > ~/.installed_software/yay_installed.txt & # Update the yay installed list

#### KEYBOARD ####
numlockx & # Activate numlock
setxkbmap -option compose:ralt & # Activate the right alt composer

signal-desktop &
