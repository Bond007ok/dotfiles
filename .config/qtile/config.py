from settings.keys import mod, terminal, keys
from settings.groups import groups
from settings.layouts import layouts, floating_layout
from settings.screens import widget_defaults, extension_defaults
from settings.screens import screens
from settings.mouse import mouse

from libqtile import hook
import os
import subprocess

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
    lazy.restart()


main = None
dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
wmname = "LG3D"
