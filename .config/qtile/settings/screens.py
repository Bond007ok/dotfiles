import os
import socket

from libqtile import bar, widget
from libqtile.config import Screen

from settings.colors import colors
from settings.keys import terminal

#### MOUSE CALLBACKS ####


def open_htop_cpu(qtile):
    """Open htop with CPU preferences in new terminal"""
    # qtile.cmd_spawn(f"{terminal} -e htop -s PERCENT_CPU")
    qtile.cmd_spawn(f"{terminal} -e bashtop")


def open_terminal(qtile):
    """Open htop with CPU preferences in new terminal"""
    qtile.cmd_spawn(f"{terminal}")


def open_htop_mem(qtile):
    """Open htop with MEMORY preferences in new terminal"""
    # qtile.cmd_spawn(f"{terminal} -e htop -s PERCENT_MEM")
    qtile.cmd_spawn(f"{terminal} -e bashtop")


def open_iftop(qtile):
    """Open iftop in new terminal"""
    qtile.cmd_spawn(f"{terminal} -e gksu bandwhich")


def open_df(qtile):
    """Open infinite custom df in new terminal"""
    qtile.cmd_spawn(f"{terminal} -e watch -c pydf -h")


def open_pavucontrol(qtile):
    """Open pavucontrol"""
    qtile.cmd_spawn('pavucontrol')


def open_psensor(qtile):
    """Open psensor (twice)"""
    qtile.cmd_spawn('psensor')
    qtile.cmd_spawn('psensor')


def open_pacsyu(qtile):
    """Open 'gksu pacman -Syyu' in new terminal"""
    qtile.cmd_spawn(f"{terminal} -e gksu pacman -Syyu")


prompt_text = "{}@{}: ".format(os.environ["USER"], socket.gethostname())

normal_font = 'Fira Code Nerd Font Mono'
normal_icon_font = 'Mononoki Nerd Font'

# normal_fontsize = 15
normal_fontsize = 11
# normal_icon_fontsize = normal_fontsize + 6
normal_icon_fontsize = normal_fontsize + 5
# normal_sep_padding = 14
normal_sep_padding = 5

kernel_version = os.popen('uname -r').readlines()[0].strip('\n')

widget_defaults = {
    'font': normal_font,
    'fontsize': normal_fontsize,
    'padding': 3
}

extension_defaults = widget_defaults.copy()

def panel_color_separator():
    return widget.Sep(
        linewidth=0, padding=normal_sep_padding, background=colors["panel_background"]
    )


def odd_color_separator():
    return widget.Sep(
        linewidth=0, padding=normal_sep_padding, background=colors["odd_widgets"]
    )

def title_bar():
    return bar.Bar( [
            widget.Sep(
                linewidth=0,
                padding=800,
                background=colors["panel_background"]
            ),

            widget.WindowName(
                foreground=colors["window_name_and_sep"],
                background=colors["panel_background"],
                font=normal_font,
                fontsize=normal_fontsize,
                padding=0
            ),
        ],
        16
    )


def even_color_separator():
    return widget.Sep(
        linewidth=0, padding=normal_sep_padding, background=colors["even_widgets"]
    )


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth=0,
                    padding=4,
                    background=colors["panel_background"]
                ),

                widget.TextBox(
                    foreground="#1793d1",
                    background=colors["panel_background"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize + 2,
                    fmt="",
                    padding=9,
                    mouse_callbacks={'Button1': open_terminal,
                                     'Button2': open_terminal}
                ),

                widget.Sep(
                    linewidth=0,
                    padding=8,
                    background=colors["panel_background"]
                ),

                widget.GroupBox(
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    margin_y=3,
                    margin_x=0,
                    padding_y=5,
                    padding_x=3,
                    borderwidth=3,
                    active=colors["font_group_names"],
                    inactive=colors["font_group_names"],
                    rounded=False,
                    highlight_color=colors["first_background"],
                    highlight_method="line",
                    this_current_screen_border=colors["border_line_current_tab"],
                    this_screen_border=colors["odd_widgets"],
                    other_current_screen_border=colors["panel_background"],
                    other_screen_border=colors["panel_background"],
                    foreground=colors["font_group_names"],
                    background=colors["panel_background"],
                    disable_drag=True
                ),
                widget.Prompt(
                    prompt=prompt_text,
                    font=normal_font,
                    fontsize=normal_fontsize,
                    padding=10,
                    foreground=colors["border_line_current_tab"],
                    background=colors["first_background"]
                ),
                widget.Sep(
                    linewidth=0,
                    padding=40,
                    foreground=colors["font_group_names"],
                    background=colors["panel_background"]
                ),

                # Thermal sensors
                odd_color_separator(),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["odd_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize + 2,
                    fmt=" ",
                ),
                widget.TextBox(
                    background=colors["odd_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    fmt=f"{kernel_version}",
                ),
                odd_color_separator(),

                # Packages
                even_color_separator(),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["even_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize + 1,
                    fmt=" ",
                    mouse_callbacks={'Button1': open_pacsyu,
                                     'Button2': open_pacsyu}
                ),
                widget.CheckUpdates(
                    background=colors["even_widgets"],
                    font=normal_font,
                    distro='Arch_yay',
                    display_format='{updates}',
                    custom_command='checkupdates+aur',
                    fontsize=normal_fontsize,
                    no_update_string='0',
                    update_interval=10,
                    mouse_callbacks={'Button1': open_pacsyu,
                                     'Button2': open_pacsyu}
                ),
                even_color_separator(),

                # Pavucontrol
                odd_color_separator(),
                widget.Volume(
                    background=colors["odd_widgets"],
                    font=normal_icon_font,
                    emoji=True,
                    fontsize=normal_icon_fontsize,
                    mouse_callbacks={
                        'Button1': open_pavucontrol, 'Button2': open_pavucontrol}
                ),
                widget.Volume(
                    background=colors["odd_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    mouse_callbacks={
                        'Button1': open_pavucontrol, 'Button2': open_pavucontrol}
                ),
                odd_color_separator(),

                # Thermal sensors
                even_color_separator(),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["even_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize + 1,
                    fmt=" ",
                    mouse_callbacks={'Button1': open_psensor,
                                     'Button2': open_psensor}
                ),
                widget.ThermalSensor(
                    background=colors["even_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    treshold=30,
                    mouse_callbacks={'Button1': open_psensor,
                                     'Button2': open_psensor}
                ),
                even_color_separator(),

                # Cpu
                odd_color_separator(),
                widget.CPUGraph(
                    background=colors["odd_widgets"],
                    border_color=colors["font_group_names"],
                    fill_color=colors["font_group_names"],
                    graph_color=colors["font_group_names"],
                    mouse_callbacks={'Button1': open_htop_cpu,
                                     'Button2': open_htop_cpu}
                ),
                widget.CPU(
                    background=colors["odd_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    format="{load_percent}%",
                    mouse_callbacks={'Button1': open_htop_cpu,
                                     'Button2': open_htop_cpu}
                ),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["odd_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    fmt=" ",
                    mouse_callbacks={'Button1': open_htop_cpu,
                                     'Button2': open_htop_cpu}
                ),
                odd_color_separator(),

                # Net
                even_color_separator(),
                widget.TextBox(
                    foreground=colors["download_icon"],
                    background=colors["even_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    fmt=" ",
                    mouse_callbacks={
                        'Button1': open_iftop, 'Button2': open_iftop}
                ),
                widget.Net(
                    background=colors["even_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    format='{down}',
                    mouse_callbacks={
                        'Button1': open_iftop, 'Button2': open_iftop}
                ),
                even_color_separator(),
                widget.TextBox(
                    foreground=colors["upload_icon"],
                    background=colors["even_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    fmt=" ",
                    mouse_callbacks={
                        'Button1': open_iftop, 'Button2': open_iftop}
                ),
                widget.Net(
                    background=colors["even_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    format='{up}',
                    mouse_callbacks={'Button1': open_iftop, 'Button2': open_iftop}
                ),
                even_color_separator(),


                # Memory
                # odd_color_separator(),
                # widget.TextBox(
                    # foreground=colors["default_icon"],
                    # background=colors["odd_widgets"],
                    # font=normal_icon_font,
                    # fontsize=normal_icon_fontsize,
                    # fmt=" ",
                    # mouse_callbacks={'Button1': open_htop_mem, 'Button2': open_htop_mem}
                # ),
                # widget.Memory(
                    # background=colors["odd_widgets"],
                    # font=normal_font,
                    # fontsize=normal_fontsize,
                    # format="{MemUsed}M",
                    # mouse_callbacks={'Button1': open_htop_mem,
                                     # 'Button2': open_htop_mem}
                # ),
                # odd_color_separator(),

                # Disk free (ROOT)
                even_color_separator(),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["even_widgets"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    fmt=" ",
                    mouse_callbacks={'Button1': open_df, 'Button2': open_df}
                ),
                widget.DF(
                    background=colors["even_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    visible_on_warn=False,
                    partition='/',
                    mouse_callbacks={'Button1': open_df, 'Button2': open_df}
                ),
                # # Disk free (HOME)
                # even_color_separator(),
                # widget.TextBox(
                    # foreground=colors["default_icon"],
                    # background=colors["even_widgets"],
                    # font=normal_icon_font,
                    # fontsize=normal_icon_fontsize,
                    # fmt=" ",
                    # mouse_callbacks={'Button1': open_df, 'Button2': open_df}
                # ),
                # widget.DF(
                    # background=colors["even_widgets"],
                    # font=normal_font,
                    # fontsize=normal_fontsize,
                    # visible_on_warn=False,
                    # partition='/home',
                    # mouse_callbacks={'Button1': open_df, 'Button2': open_df}
                # ),
                # even_color_separator(),

                # Current layout
                odd_color_separator(),
                widget.CurrentLayoutIcon(
                    background=colors["odd_widgets"],
                    font=normal_icon_font,
                    scale=0.95
                ),
                widget.CurrentLayout(
                    foreground=colors["font_group_names"],
                    background=colors["odd_widgets"],
                    font=normal_font,
                    fontsize=normal_fontsize,
                    padding=5
                ),
                odd_color_separator(),

                # Clock
                panel_color_separator(),
                widget.TextBox(
                    foreground=colors["default_icon"],
                    background=colors["panel_background"],
                    font=normal_icon_font,
                    fontsize=normal_icon_fontsize,
                    fmt=" ",
                ),
                widget.Clock(
                        foreground=colors["font_group_names"],
                        background=colors['panel_background'],
                        font=normal_font,
                        fontsize=normal_fontsize + 2,
                        format="%d-%m-%Y %H:%M"
                        ),

                # Systray
                widget.Systray(
                    background=colors["panel_background"],
                    font=normal_icon_font,
                    icon_size=13,
                    padding=3
                ),
            ],
            20,
            margin=[3, 130, 0, 130],  # N E S W,
        ),
        bottom=title_bar()
    ),
    Screen(
        bottom=title_bar()
    )
]
