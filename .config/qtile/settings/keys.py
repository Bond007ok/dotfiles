from libqtile.config import Key
from libqtile.command import lazy
import os

mod = "mod4"
#terminal = guess_terminal()
terminal = "alacritty"
home = os.path.expanduser('~')

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

keys = [Key(*key[:-1], desc=key[-1]) if type(key[-1]) == str else Key(*key)  for key in [
    ##### WINDOWS #####
    # Switch between windows in current stack pane
    ([mod], "k", lazy.layout.up(), 'Move to the UPPER window'),
    ([mod], "j", lazy.layout.down(), 'Move to the LOWER window'),
    ([mod], "l", lazy.layout.right(), 'Move to the RIGHT window'),
    ([mod], "h", lazy.layout.left(), 'Move to the LEFT window'),

    # Change window sizes (thanks OldTechBloke !)
    ([mod, "shift"], "k", 
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        'Increase the size of the current window UPWARDS'
    ),
    ([mod, "shift"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        'Increase the size of the current window DOWNWARDS'
    ),
    ([mod, "shift"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        'Increase the size of the current window RIGHTWARDS'
    ),
    ([mod, "shift"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        'Increase the size of the current window LEFTWARDS'
    ),

    # Move windows in current stack
    ([mod, "control"], "k", lazy.layout.shuffle_up(), 'Exchange this window with the UPPER one'),
    ([mod, "control"], "j", lazy.layout.shuffle_down(), 'Exchange this window with the LOWER one'),
    ([mod, "control"], "l", lazy.layout.shuffle_right(), 'Exchange this window with the RIGHT one'),
    ([mod, "control"], "h", lazy.layout.shuffle_left(), 'Exchange this window with the LEFT one'),

    # Unminimize all
    ([mod], "u", lazy.spawn("qtile-cmd -o group -f unminimize_all"), 'UNMINIMIZE all windows'),

    # Make window Fullscreen
    ([mod], "f", lazy.window.toggle_fullscreen(), '(Toggle) Make current window FULLSCREEN'),
    # Minimize current window
    ([mod, "shift"], "f", lazy.window.toggle_minimize(), '(Toggle) MINIMIZE current window'),
    # Make window floating
    ([mod, "control"], "f", lazy.window.toggle_floating(), '(Toggle) Make current window Floating'),

    # Toggle between different layouts as defined below
    ([mod], "Tab", lazy.next_layout(), 'Use the NEXT layout for this workspace'),
    ([mod, "shift"], "Tab", lazy.prev_layout(), 'Use the PREVIOUS layout for this workspace'),

    # Go to the next screen
    ([mod], "period", lazy.next_screen(), 'Go to the NEXT screen'),
    ([mod, "shift"], "period", lazy.function(window_to_next_screen), 'Move the current window to the NEXT screen'),
    # Go to the previous screen
    ([mod], "comma", lazy.prev_screen(), 'Go to the PREVIOUS screen'),
    ([mod, "shift"], "comma", lazy.function(window_to_previous_screen), 'Move the current window to the PREVIOUS screen'),

    # Kill the current window
    ([mod], "w", lazy.window.kill(), 'KILL the current window'),

    ##### SOFTWARE #####
    # Launch terminal
    ([mod], "Return", lazy.spawn(terminal), 'Launch a TERMINAL'),
    ([mod, "shift"], "Return", lazy.spawn("  gksu " + terminal), 'Launch a TERMINAL as ROOT'),

    # Open a new window of Librewolf (Browser)
    ([mod], "b", lazy.spawn("librewolf"), 'Launch (Browser) LIBREWOLF'),
    ([mod, "shift"], "b", lazy.spawn("brave"), 'Launch (Browser) BRAVE'),

    # Open Htop (Interactive process viewer)
    ([mod], "i", lazy.spawn(terminal + " -e bpytop"), 'Launch (Interactive process viewer) BPYTOP'),    # Bpytop
    ([mod, "control"], "i", lazy.spawn(terminal + " -e htop"), 'Launch (Interactive process viewer) HTOP'),    # Htop
    ([mod, "shift"], "i", lazy.spawn("stacer"), 'Launch (Interactive process viewer) STACER'),    # Stacer

    # Open VirtualBox (Virtualization)
    ([mod], "v", lazy.spawn("virt-manager"), 'Launch (Virtualization) VIRT-MANAGER'),
    ([mod, "shift"], "v", lazy.spawn("virtualbox"), 'Launch (Virtualization) VIRTUALBOX'),

    # Open ranger (file Explorer) (~/scripts/ranger has a little delay in it
    # because ranger isn't resizing very well)
    ([mod], "e", lazy.spawn(terminal + f" -e {home}/.config/qtile/settings/scripts/ranger"), 'Launch (Explorer) RANGER'),
    ([mod, "shift"], "e", lazy.spawn(terminal + f" -e doas {home}/.config/qtile/settings/scripts/ranger"), 'Launch (Explorer) RANGER as ROOT'),
    # Open thunar (file Explorer)
    ([mod, "control"], "e", lazy.spawn("thunar"), 'Launch (Explorer) THUNAR'),
    ([mod, "control", "shift"], "e", lazy.spawn("  gksu thunar"), 'Launch (Explorer) THUNAR as ROOT'),

    # Open gnome-disks (Disks)
    ([mod], "d", lazy.spawn("gnome-disks"), 'Launch (Disks) GNOME-DISKS'),
    ([mod, "shift"], "d", lazy.spawn("  gksu gparted"), 'Launch (Disks) GPARTED'),

    # Rofi related
    ([mod], "space", lazy.spawn("rofi -columns 2 -show-icons -show drun"), 'Launch ROFI APP MENU'),  # Open an app
    ([mod, "shift"], "space", lazy.spawn("rofi -show window"), 'Launch ROFI PICK WINDOW'),  # Show Windows
    ([mod, "control"], "space", lazy.spawn(f"{home}/.config/qtile/settings/scripts/rofi_power_menu"), 'Launch ROFI LOGOUT MENU'),  # Show Windows

    # Redshift (Redshift)
    ([mod], "r", lazy.spawn("redshift -O 2400"), 'Enable REDSHIFT'),    # Enable redshift
    ([mod, "shift"], "r", lazy.spawn("redshift -x"), 'KILL REDSHIT'),   # Disable/stop redshift

    # Screenshot (Screenshot)
    ([mod], "s", lazy.spawn(f"flameshot full -p {home}/Pictures/screenshots/"), 'Launch (Screenshot) FULL FLAMESHOT'),   # Automatic
    ([mod, "shift"], "s", lazy.spawn("flameshot gui"), 'Launch (Screenshot) SPECIFIC FLAMESHOT'),   # Specific

    # Change wallpaper
    ([mod], "Right", lazy.spawn("variety -p"), 'Set the PREVIOUS BACKGROUND'),   # Previous background
    ([mod], "Left", lazy.spawn("variety -n"), 'Set the NEXT BACKGROUND'),   # Next background

    # Youtube (Y)
    ([mod], "y", lazy.spawn("/opt/LBRY/lbry"), 'Lauch (Youtube) LBRY'),
    ([mod, "shift"], "y", lazy.spawn("/opt/FreeTube/freetube"), 'Launch (Youtube) FREETUBE'),

    # Graphics (G)
    ([mod], "g", lazy.spawn("xournalpp"), 'Launch (Graphics tablet) XOURNAL'),
    ([mod, "shift"], "g", lazy.spawn("otd-gui"), 'Lauch (Graphics tablet) OTD'),

    # Signal (Communications)
    ([mod], "c", lazy.spawn("signal-desktop"), 'Launch (Communication) SIGNAL'),
    # KColorChooser (Color picker)
    ([mod, "control"], "c", lazy.spawn("kcolorchooser"), 'Launch (Color picker) KCOLORCHOOSER'),

    # RSS Feeds (News)
    ([mod], "n", lazy.spawn(f"{home}/Applications/Fluent.Reader.1.0.0_453f44739224a05887f240ab362eb14e.AppImage"), 'Launch (News) FLUENT_READER'),

    # Lxappearance (Appereance)
    ([mod], "a", lazy.spawn("lxappearance"), 'Launch (Appereance) LXAPPEREANCE'),
    ([mod, "shift"], "a", lazy.spawn("  gksu lxappearance"), 'Launch (Appereance) LXAPPEREANCE as ROOT'),

    # Pulsemixer (pulsemixer)
    ([mod], "p", lazy.spawn(terminal + " -e pulsemixer"), 'Launch (Pulsemixer) Pulsemixer'),
    ([mod, "control"], "p", lazy.spawn("/var/lib/portmaster/portmaster-start app --data=/var/lib/portmaster"), 'Launch (Portmaster) Portmaster'),    # PortMaster

    # Translator
    ([mod], "t", lazy.spawn("argos-translate-gui"), 'Launch (Translator) ARGOS TRANSLATE'), # Translator

    # Lock screen (Lock)
    ([mod, 'shift', 'control'], "l", lazy.spawn("xsecurelock"), 'Launch (Lock) XSECURELOCK'),

    ##### HARDWARE CONFIGS #####
    # Volume for Headphones
    ([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 1 +5%"), '+5% Volume HEADPHONES'),    # +5% Volume
    ([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 1 -5%"), '-5% Volume HEADPHONES'),   # -5% Volume
    ([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 1 toggle"), '(Toggle) MUTE HEADPHONES'),   # toggle Mute
    # Volume for HDMI
    (["shift"], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 0 +5%"), '+5% Volume HDMI'),    # +5% Volume
    (["shift"], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 0 -5%"), '-5% Volume HDMI'),   # -5% Volume
    (["shift"], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 0 toggle"), '(Toggle) MUTE HDMI'),   # toggle Mute

    # Brightness
    # ([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
    # ([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),

    ##### DIRECT QTILE #####
    # Restart Qtile
    ([mod, "control"], "r", lazy.restart()),
    # Shutdown Qtile
    ([mod, "control"], "q", lazy.shutdown()),


]]

def show_keys(keys):
  """
  print current keybindings in a pretty way for a rofi/dmenu window.
  """
  processed_str = ""
  ignored_keys = (
      "XF86AudioMute",  #
      "XF86AudioLowerVolume",  #
      "XF86AudioRaiseVolume",  #
      "XF86AudioPlay",  #
      "XF86AudioNext",  #
      "XF86AudioPrev",  #
      "XF86AudioStop",
  )
  keys_to_replace = {
      "mod4": "[S]",  #
      "control": "[Ctl]",  #
      "mod1": "[Alt]",  #
      "shift": "[Shft]",  #
      "twosuperior": "²",  #
      "less": "<",  #
      "ampersand": "&",  #
      "Escape": "Esc",  #
      "Return": "Enter",  #
  }
  motion_keys = [letter.upper() for letter in ['k', 'j', 'l', 'h']]

  previous_key = ''
  for key in keys:
    if key.key in ignored_keys:
      continue


    raw_key = key.key.title()
    # Get the index of the raw_key in motion_keys
    index = motion_keys.index(raw_key) if raw_key in motion_keys else None
    # Get the index of the previous key in motion_keys
    previous_motion_key = motion_keys[index - 1] if index else None

    # If the 2 keys (previous and current) aren't the same and they aren't
    # following themselves in the motion_keys list (and if there wasn't a
    # previous key)
    if previous_key != raw_key and previous_motion_key != previous_key and previous_key != '':
        processed_str += '\n'
    previous_key = raw_key

    modifiers = ""
    key_name = ""
    description = key.desc
    for modifier in key.modifiers:
      if modifier in keys_to_replace.keys():
        modifiers += keys_to_replace[modifier] + " + "
      else:
        modifiers += modifier.capitalize() + " + "

    if len(key.key) > 1:
      if key.key in keys_to_replace.keys():
        key_name = keys_to_replace[key.key]
      else:
        key_name = key.key.title()
    else:
      key_name = key.key

    current_key_processed_line = "{:<30} {}\n".format(modifiers + key_name, description)
    processed_str += current_key_processed_line

  return processed_str

keys.extend(
    [Key([mod], "F1", lazy.spawn("sh -c 'echo \"" + show_keys(keys) + "\" | rofi -dmenu -i -mesg \"Keyboard shortcuts:\nKEYS                          ACTION\"'"), desc="Print keyboard bindings")]
)
