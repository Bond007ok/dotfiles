from libqtile import layout
from settings.colors import colors

layout_theme = {
    'border_width': 2,
    'margin': 2,
    'border_focus': colors["window_border_focus"][0],
    'border_normal': colors["window_border_normal"][0],
}

layouts = [
    #layout.Stack(stacks=2, **layout_theme),
    # layout.Columns(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.Zoomy(**layout_theme),

    layout.Bsp(**layout_theme),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.Max(**layout_theme),
    # layout.TreeTab(
    #    font = 'Ubuntu',
    #    fontsize = 10,
    #    sections = ['FIRST', 'SECOND'],
    #    section_fontsize = 11,
    #    bg_color = '141414',
    #    active_bg = '90C435',
    #    active_fg = '000000',
    #    inactive_bg = '384323',
    #    inactive_fg = 'a0a0a0',
    #    padding_y = 5,
    #    section_top = 10,
    #    panel_width = 320,
    #    **layout_theme
    #    ),
    # layout.Floating(**layout_theme)
]

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'display'},
    {'wname': 'dragon'},
    {'wname': 'Mullvad VPN'},
    {'wname': 'Confirm to replace files'},
    {'wname': 'File Operation Progress'},
    {'wmclass': 'lxpolkit'},
    {'wmclass': 'pentablet'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
