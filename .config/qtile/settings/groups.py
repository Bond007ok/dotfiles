from libqtile.config import Key, Group
from libqtile.command import lazy
from settings.keys import mod, keys

group_names = [
    '   ', '   ', '   ',
    '   ', '   ', '   '
]

groups = [Group(name) for name in group_names]

# Add the key shorcuts
for num, group in enumerate(groups, 1):
    keys.extend([
        # mod1 + number of group = switch to group
        Key([mod], str(num), lazy.group[group.name].toscreen(),
            desc='Switch to group {}'.format(group.name)),

        # mod1 + shift + letter of group = move focused window to group
        Key([mod, 'shift'], str(num), lazy.window.togroup(group.name, switch_group=False),
            desc='Switch to & move focused window to group {}'.format(group.name)),
    ])
