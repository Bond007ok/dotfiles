colors = {
    # panel background  #292d3e
    "panel_background": "#1e212d",
    # prompt and current workspace background color
    "first_background": "#434758",
    # font color for group names
    "font_group_names": "#ffffff",
    # border line color for current tab
    "border_line_current_tab": "#ff5555",

    # The following should be COMPLEMENTARY colors (to improve sightseeing)
    # border line color for other tab and odd widgets 8d62a9 996A00
    "odd_widgets": "#444D43",
    # color for the even widgets 668bd7
    "even_widgets": "#312646",

    # window name e1acff
    "window_name_and_sep": "#E6B629",
    # Other font color
    "other_font": "#50bcd8",
    "upload_icon": "#265773",
    "download_icon": "#734026",
    "default_icon": "#59c2ff",

    "window_border_focus": "#46d9ff",
    "window_border_normal": "#282c34"
}

colors = {key: 2 * [color] for key, color in colors.items()}
