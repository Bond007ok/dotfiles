#### ALIASES ####

# navigation
alias .. 'cd ..'
alias ... 'cd ../..'
alias .3 'cd ../../..'
alias .4 'cd ../../../..'
alias .5 'cd ../../../../..'

# confirm before overwriting something
alias cp "cp -i"
alias mv 'mv -i'
alias rm 'rm -i'

# Changing "ls" to "exa"
alias ls 'exa -alh --color=always --group-directories-first --icons'
alias lt 'exa -T --color=always --group-directories-first --icons'
alias lr 'exa -aR --color=always --group-directories-first --icons'

# Changing exa
alias exa 'exa -alh --color=always --group-directories-first --icons'

# Color cat
alias cat 'bat'

# Change "du" to "dust"
alias du 'dust'

# nvim to vim
alias vim 'echo "use \'v\' !!"'
alias v 'nvim'

# Pydf
alias df 'pydf -h'

# pacman and yay
alias pacsyu 'sudo pacman -Syyu'                 # update only standard pkgs
alias yay 'paru'																# Not for me, but just for copying and pasting commands
alias paru 'paru --sudo /bin/sudo'


# Ipython
alias python 'ipython3'

# the terminal rickroll
alias rr 'curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

## get top process eating memory
alias psmem 'ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu 'ps auxf | sort -nr -k 3 | head -10'

# youtube-dl
alias yta-aac "youtube-dl --extract-audio --audio-format aac"
alias yta-best "youtube-dl --extract-audio --audio-format best"
alias yta-flac "youtube-dl --extract-audio --audio-format flac"
alias yta-m4a "youtube-dl --extract-audio --audio-format m4a"
alias yta-mp3 "youtube-dl --extract-audio --audio-format mp3"
alias yta-opus "youtube-dl --extract-audio --audio-format opus"
alias yta-vorbis "youtube-dl --extract-audio --audio-format vorbis"
alias yta-wav "youtube-dl --extract-audio --audio-format wav"
alias ytv-best "youtube-dl -f bestvideo+bestaudio"

alias youtube-dl "youtube-dl --prefer-free-formats"

# Colors
alias watch "watch -c"
alias less "less -r"

# Ping Test
alias pt "ping metager.org"

# Lsblk
alias lsblk "lsblk -o NAME,FSTYPE,PARTTYPENAME,FSVER,LABEL,MODEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINT,TRAN,STATE,PTTYPE"
alias lablk "lsblk -o NAME,FSTYPE,PARTTYPENAME,FSVER,LABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINT,TRAN,STATE,PTTYPE,MODEL" # Little common mistake

# RipGrep
alias rg 'rg --hidden --follow --pretty --smart-case --sort path'
alias grep 'rg --hidden --follow --pretty --smart-case --sort path'

# Find
alias fd 'fd --hidden --no-ignore --ignore-case'
alias find 'fd --hidden --no-ignore --ignore-case'

# Git diff
alias gitd 'git diff | diff-so-fancy | less'

alias r 'ranger'

# Argos translate
alias argos-en-es 'argos-translate --from en --to es'
alias argos-en-fr 'argos-translate --from en --to fr'
alias argos-es-en 'argos-translate --from es --to en'
alias argos-es-fr 'argos-translate --from es --to fr'
alias argos-fr-en 'argos-translate --from fr --to en'
alias argos-fr-es 'argos-translate --from fr --to es'
