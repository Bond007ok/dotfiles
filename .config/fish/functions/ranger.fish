function ranger
    set dir (mktemp -t ranger_cd.XXX)
    /bin/ranger --choosedir=$dir
    cd (cat $dir) $argv
    rm -f $dir
    commandline -f repaint
end
