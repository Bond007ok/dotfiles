### FUNCTIONS ###
# Function for printing a column (splits input on whitespace)
# ex: echo 1 2 3 | coln 3
# output: 3
function coln
    while read -l input
        echo $input | awk '{print $'$argv[1]'}'
    end
end

# Function for printing a row
# ex: seq 3 | rown 3
# output: 3
function rown --argument index
    sed -n "$index p"
end

# Function for ignoring the first 'n' lines
# ex: seq 10 | skip 5
# results: prints everything but the first 5 lines
function skip --argument n
    tail +(math 1 + $n)
end

# Function for taking the first 'n' lines
# ex: seq 10 | take 5
# results: prints only the first 5 lines
function take --argument number
    head -$number
end
### END OF FUNCTIONS ###


### BASIC ###
set fish_greeting # Supress the fish's intro message

# Happy Birthday!
# toilet --width 500 --metal "Happy Birthday!!!" | lolcat

# Set custom vi keybindings
function my_vi_bindings
  fish_vi_key_bindings # Enable fish's vim keybindings
  bind -M insert -m default kj backward-char force-repaint # Enable kj as escape to enter normal mode

  bind yy fish_clipboard_copy # Use the system's clipboard
  bind p fish_clipboard_paste # Use the system's clipboard
end
set -g fish_key_bindings my_vi_bindings

### END OF BASIC ###

# Auto cd
function ranger
    set dir (mktemp -t ranger_cd.XXX)
    /bin/ranger --choosedir=$dir
    cd (cat $dir) $argv
    rm -f $dir
    commandline -f repaint
end
funcsave ranger

#### VARIABLES ####

set -x EDITOR "/usr/bin/nvim"
set -x HISTCONTROL "ignoreboth"
set -x MANPAGER "nvim -u ~/.config/nvim/init.vim -c 'set ft=man' -"

# Disable lesshist
set LESSHISTFILE -

# Aliases
source ~/.config/fish/general/aliases.fish

# Start-up command
#neofetch

# z.lua
# lua ~/.config/fish/scripts/z.lua --init fish > ~/.config/fish/conf.d/z.fish

# Set an alacritty theme with 'alacritty-themes'

### STARSHIP ###
starship init fish | source
### END STARSHIP ###
